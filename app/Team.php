<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

    protected $fillable = ['name'];
    /**
     * RELATIONSHIP METHODS
     */

     public function users(){
         return $this->hasMany(User::class);
     }
}
