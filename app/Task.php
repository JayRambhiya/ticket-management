<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Task extends Model
{

    protected $fillable = ['name','assigned_to','created_by'];
    /**
     * RELATIONSHIP METHODS
     */


     public function user()
     {
         return $this->belongsTo(User::class,'assigned_to');
     }

     public function statuses()
     {
         return $this->hasMany(Status::class);
     }

     /**
      * ACCESSOR METHODS
      */

      public function getStatusAttribute()
      {
          return $this->statuses()->latest()->first();
      }

     /**
      * HELPER METHODS
      */

      public function getTeamLeader(Task $task)
      {
        $team_leader = User::where('id',$task->created_by)->get();
        return $team_leader;
      }

}
