<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * RELATIONSHIP METHODS
     */

     public function tasks(){
         return $this->hasMany(Task::class,'assigned_to');
     }

     public function team(){
         return $this->belongsTo(Team::class);
     }

     public function statuses()
     {
         return $this->hasMany(Status::class);
     }

     /**
      * HELPER FUNCTIONS
      */
    public function getLeader(User $user)
    {
        $team_leader = User::where('team_id',$user->team_id)->where('role','leader')->get();
        return $team_leader;
    }
}
