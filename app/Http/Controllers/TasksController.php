<?php

namespace App\Http\Controllers;

use App\Task;
use App\Status;
use App\User;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Controllers\session;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TasksController extends Controller
{
  
    public function index()
    {
        $tasks = collect();
        if(auth()->user()->role === 'member')
       {
            $tp = Task::where('assigned_to',auth()->id())->get();
            foreach($tp as $task){
                if($task->status->resolved_at == null && $task->status->gave_up_at == null){
                    // dd($task);
                    $tasks->push($task);
                } 
            }
            // dd($tasks);
        }else{
            $tp = Task::where('created_by',auth()->id())->get();
            foreach($tp as $task){
                // echo($task->statuses);
                // echo("<br><br>");
                if($task->status->resolved_at === null){
                        $tasks->push($task);
                } 
            }
        }
        // dd($tasks);
        return view('tasks.index',compact([
            'tasks'
        ]));
    }

 
    public function create()
    {
        $users = User::where('team_id',auth()->user()->team_id)->get();
        // dd($users);
        return view('tasks.create',compact([
            'users'
        ]));
    }
    public function store(CreateTaskRequest $request)
    {
        // dd('reached');
        $task = Task::create([
            'name' => $request->name,
            'assigned_to' => $request->assigned_to,
            'created_by' => auth()->id()
        ]);

        Status::create([
            'task_id' => $task->id,
            'user_id' => $request->assigned_to
        ]);
        
        session()->flash('success','Task created Successfully');
        return redirect(route('tasks.index'));
        
    }

  
    public function show(Task $task)
    {
        
    }

  
    public function edit(Task $task)
    {
        $users = User::where('team_id',auth()->user()->team_id)->get();
        return view('tasks.edit',compact([
            'users',
            'task'
        ]));
    }

  
    public function update(UpdateTaskRequest $request, Task $task)
    {
        $task->name = $request->name;
        $task->assigned_to = $request->assigned_to;
        // $task->statuses->task_id = $task->id;
        $task->save();
        $status = $task->status;
        // dd($status);
        $status->update([
            'user_id' => $request->assigned_to
        ]);
    
        session()->flash('success','Task updated Successfully');
        return redirect(route('tasks.index'));
    }

  
    public function destroy(Task $task)
    {
        
    }

    public function resolve(Task $task)
    {
        $status = $task->status;
        // dd($status);
        $status->update([
            'resolved_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
        // dd('done');
        session()->flash('success','Task has been resolved Successfully');
        return redirect(route('tasks.index'));
    }

    public function giveUp(Task $task)
    {
        $status = $task->status;
        // dd($status);
        $status->update([
            'gave_up_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
        // dd('done');
        session()->flash('success','Task has been given up Successfully');
        return redirect(route('tasks.index'));
    }

    public function resolvedTasks()
    {   
        $tasks = collect();
        $tp = Task::where('created_by',auth()->id())->get();
        // dd($tp);
        foreach($tp as $task){
            // echo($task->statuses);
            // echo("<br><br>");
            if($task->status->resolved_at !== null && $task->status->approved_at === null){
                    $tasks->push($task);
            } 
        }
        // dd($tasks);
        return view('tasks.resolved.index',compact([
            'tasks'
        ]));
    }

    public function approveTask(Task $task)
    {
        $status = $task->status;
        // dd($status);
        $status->update([
            'approved_at' =>  \Carbon\Carbon::now()->toDateTimeString(),
        ]);
    
        session()->flash('success','Task approved Successfully');
        return redirect(route('tasks.index'));
    }

    public function unapproveTask(Task $task)
    {
        $status = $task->status;
        // dd($status);
        $status->update([
            'resolved_at' =>  null,
        ]);
    
        session()->flash('success','Task unapproved Successfully');
        return redirect(route('tasks.index'));
    }

    public function approvedTasks()
    {   
        $tasks = collect();
        $tp = Task::where('created_by',auth()->id())->get();
        // dd($tp);
        foreach($tp as $task){
            // echo($task->statuses);
            // echo("<br><br>");
            if($task->status->approved_at !== null && $task->status->resolved_at !== null){
                    $tasks->push($task);
            } 
        }
        // dd($tasks);
        return view('tasks.approved.index',compact([
            'tasks'
        ]));
    }

    public function selfReassignTask(Task $task)
    {
        $users = User::where('team_id',auth()->user()->team_id)->where('id','!=',$task->assigned_to)->get();
        // dd($users);
        return view('tasks.re-assign.reassign',compact([
            'users',
            'task'
        ]));
    }

    public function updateSelfReassignTask(UpdateTaskRequest $request,Task $task)
    {
        $task->assigned_to = $request->assigned_to;
        $task->save();

        Status::create([
            'task_id' => $task->id,
            'user_id' => $request->assigned_to
        ]);
    
        session()->flash('success','Task Re-Assigned Successfully');
        return redirect(route('tasks.index'));   
    }

    public function systemReassignTask(Task $task)
    {
        $users = User::where('team_id',auth()->user()->team_id)->where('role','!=','leader')->where('id','!=',$task->assigned_to)->get();
        // dd($users);
        $free_users = collect();
        foreach($users as $user){
            if($user->tasks()->count() == 0){
                $free_users->push($user);
            }
        }
        // dd($free_users);
        if($users->count() == 1)
        {   
            $task->assigned_to = $users->id;
            $task->save();
            Status::create([
                'task_id' => $task->id,
                'user_id' => $users->id
            ]);

            session()->flash('success','Task Re-Assigned by the system Successfully');
            return redirect(route('tasks.index'));
        }else if($users->count() > 1 && $free_users->count() > 0){
            $task->assigned_to = $free_users->first()->id;
            $task->save();

            Status::create([
                'task_id' => $task->id,
                'user_id' => $free_users->first()->id
            ]);

            session()->flash('success','Task Re-Assigned by the system Successfully');
            return redirect(route('tasks.index'));
        }else if($users->count() > 1 && $free_users->count() == 0){
            // dd('reached');
            $previous_week_first_date = Carbon::parse('2020-07-08 00:00')->startOfWeek()->subWeek();
            $previous_week_last_date = Carbon::parse('2020-07-08 00:00')->startOfWeek();
            // dd($previous_week_last_date);
            $effecient_user;
            $highest_completed_task_count = 0;
            foreach($users as $user)
            {
                $completed_task_count = $user->statuses()->where('approved_at','<',$previous_week_last_date)->where('approved_at','>=',$previous_week_first_date)->get()->count();
                // dd($completed_task_count);
                if($completed_task_count >= $highest_completed_task_count)
                {
                    $highest_completed_task_count = $completed_task_count;
                    $effecient_user  = $user;
                }
            }
            // dd($effecient_user);
            $task->assigned_to = $effecient_user->id;
            $task->save();
            
            Status::create([
                'user_id' => $effecient_user->id,
                'task_id' => $task->id
            ]);

            session()->flash('success','Task Re-Assigned by the system Successfully');
            return redirect(route('tasks.index'));
        }
    }
}
