<?php

namespace App\Http\Controllers;

use App\User;
use App\Team;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Carbon\Carbon;

class UsersController extends Controller
{
    
    public function index()
    {
        if(auth()->user()->role === 'leader'){
            // dd('reached');
            $users = User::where('team_id',auth()->user()->team_id)->get();
        }else{
            $users = User::where('role','!=','manager')->get();
        }

        return view('users.index',compact([
            'users'
        ]));
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(CreateUserRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' =>  \Illuminate\Support\Facades\Hash::make($request->password)
        ]);

        session()->flash('success','User created Successfully');
        return redirect(route('users.index'));

    }

    
    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        $teams = Team::all();
        // dd($teams);
        return view('users.edit',compact([
            'teams',
            'user'
        ]));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        // dd($request);
        
            $user->name = $request->name;
            $user->email = $request->email;
            $user->team_id = $request->team_id;

            $user->save();

        session()->flash('success','User edited Successfully');
        return redirect(route('users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function bestEmployee(User $user)
    {
        $date = Carbon::parse('2020-08-15');
        $myMonth = $date->month - 1;
        $start_month = Carbon::parse('2020-'. $myMonth .'-1 00:00');
        // echo($start_month);
        $end_month = Carbon::parse('2020-'. $myMonth .'-1 00:00')->addMonth()->subDay();
        // dd($end_month);

        $users = User::where('role','!=','manager')->where('role','!=','leader')->get();
        $effecient_user;
        $highest_completed_task_count = 0;
        foreach($users as $user)
        {
            // echo($user->statuses);
            $completed_task_count = $user->statuses()->where('approved_at','>=',$start_month)->where('approved_at','<=',$end_month)->get()->count();
            // dd($completed_task_count);
            if($completed_task_count >= $highest_completed_task_count)
            {
                $highest_completed_task_count = $completed_task_count;
                $effecient_user  = $user;
            }
        }
        dd($effecient_user);
    }
}
