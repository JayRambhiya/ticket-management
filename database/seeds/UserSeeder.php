<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $team_1 = \App\Team::create([
            'name' => 'Orange',
        ]);

        $team_2 = \App\Team::create([
            'name' => 'Yellow',
        ]);

        $team_3 = \App\Team::create([
            'name' => 'Blue',
        ]);

        App\User::create([
            'name' => 'Himanshu Thakkar',
            'email' => 'HT@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'manager'
        ]);

        $team_leader_1 = \App\User::create([
            'name' => 'Vidhi Parikh',
            'email' => 'vidhi@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'leader',
            'team_id'=>$team_1->id
        ]);


        $team_leader_2 = \App\User::create([
            'name' => 'Jay Rambhiya',
            'email' => 'jay@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'leader',
            'team_id'=>$team_2->id
        ]);

        $team_leader_3 = \App\User::create([
            'name' => 'Shubh Kothari',
            'email' => 'shubh@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'leader',
            'team_id'=>$team_3->id
        ]);

        $team_member_1 = \App\User::create([
            'name' => 'Ayush Shah',
            'email' => 'ayush@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_1->id
        ]);

        $team_member_2 = \App\User::create([
            'name' => 'Yash Parekh',
            'email' => 'yash@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_1->id
        ]);

        $team_member_3 = \App\User::create([
            'name' => 'Riddesh Shah',
            'email' => 'ridz@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_1->id
        ]);

        $team_member_4 = \App\User::create([
            'name' => 'Sahil Shah',
            'email' => 'sahil@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_1->id
        ]);

        $team_member_5 = \App\User::create([
            'name' => 'Shraddha Bhosle',
            'email' => 'shraddha@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_1->id
        ]);

        $team_member_6 = \App\User::create([
            'name' => 'Biswa Rath',
            'email' => 'biswa@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_2->id
        ]);

        $team_member_7 = \App\User::create([
            'name' => 'Anurag Bassi',
            'email' => 'anurag@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_2->id
        ]);

        $team_member_8 = \App\User::create([
            'name' => 'Ankur Tiwari',
            'email' => 'ankur@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_2->id
        ]);

        $team_member_9 = \App\User::create([
            'name' => 'Sonam Shak',
            'email' => 'sonam@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_2->id
        ]);

        $team_member_10 = \App\User::create([
            'name' => 'Deepika Singh',
            'email' => 'deepika@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_2->id
        ]);

        $team_member_11 = \App\User::create([
            'name' => 'Ranbir Kapoor',
            'email' => 'ranbir@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_3->id
        ]);

        $team_member_12 = \App\User::create([
            'name' => 'Joffrey Doe',
            'email' => 'joffery@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_3->id
        ]);

        $team_member_13 = \App\User::create([
            'name' => 'Jamie Doe',
            'email' => 'jamie@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_3->id
        ]);

        $team_member_14 = \App\User::create([
            'name' => 'Jane Doe',
            'email' => 'jane@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_3->id
        ]);

        $team_member_15 = \App\User::create([
            'name' => 'John Doe',
            'email' => 'john@studylinkclasses.com',
            'password' => \Illuminate\Support\Facades\Hash::make('abcd1234'),
            'role' => 'member',
            'team_id'=>$team_3->id
        ]);

        $task_1 = \App\Task::create([
            'name' => 'Design database for project 1',
            'assigned_to' => $team_member_1->id,
            'created_by' => $team_leader_1->id,
        ]);

        $task_2 = \App\Task::create([
            'name' => 'Design UI for project 1',
            'assigned_to' => $team_member_2->id,
            'created_by' => $team_leader_1->id,
        ]);

        $task_3 = \App\Task::create([
            'name' => 'Design Core logic for project 1',
            'assigned_to' => $team_member_3->id,
            'created_by' => $team_leader_1->id,
        ]);

        $task_4 = \App\Task::create([
            'name' => 'Create a server for project 1',
            'assigned_to' => $team_member_4->id,
            'created_by' => $team_leader_1->id,
        ]);

        $task_5 = \App\Task::create([
            'name' => 'Design database for project 2',
            'assigned_to' => $team_member_5->id,
            'created_by' => $team_leader_1->id,
        ]);

        $task_6 = \App\Task::create([
            'name' => 'Design UI for project 2',
            'assigned_to' => $team_member_6->id,
            'created_by' => $team_leader_2->id,
        ]);

        $task_7 = \App\Task::create([
            'name' => 'Design core logic for project 2',
            'assigned_to' => $team_member_7->id,
            'created_by' => $team_leader_2->id,
        ]);

        $task_8 = \App\Task::create([
            'name' => 'Create a server for project 2',
            'assigned_to' => $team_member_7->id,
            'created_by' => $team_leader_2->id,
        ]);

        $task_9 = \App\Task::create([
            'name' => 'Design database for project 3',
            'assigned_to' => $team_member_11->id,
            'created_by' => $team_leader_3->id,
        ]);
        $task_10 = \App\Task::create([
            'name' => 'Design UI for project 3',
            'assigned_to' => $team_member_11->id,
            'created_by' => $team_leader_3->id,
        ]);
        $task_11 = \App\Task::create([
            'name' => 'Design core logic for project 3',
            'assigned_to' => $team_member_12->id,
            'created_by' => $team_leader_3->id,
        ]);
        $task_12 = \App\Task::create([
            'name' => 'Create server for project 3',
            'assigned_to' => $team_member_12->id,
            'created_by' => $team_leader_3->id,
        ]);

        \App\Status::create([
            'user_id' => $team_member_1->id,
            'task_id' => $task_1->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_2->id,
            'task_id' => $task_2->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_3->id,
            'task_id' => $task_3->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_4->id,
            'task_id' => $task_4->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_5->id,
            'task_id' => $task_5->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_3->id,
            'task_id' => $task_6->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_4->id,
            'task_id' => $task_7->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_4->id,
            'task_id' => $task_8->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_5->id,
            'task_id' => $task_9->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_5->id,
            'task_id' => $task_10->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_6->id,
            'task_id' => $task_11->id
        ]);

        \App\Status::create([
            'user_id' => $team_member_6->id,
            'task_id' => $task_12->id
        ]);
    }
}
