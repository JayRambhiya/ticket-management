<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tasks/resolved','TasksController@resolvedTasks')->name('tasks.resolved');
Route::get('/tasks/approved','TasksController@approvedTasks')->name('tasks.approved');
Route::get('/tasks/{task}/self_assign','TasksController@selfReassignTask')->name('tasks.self_assign');
Route::get('/users/best_employee','UsersController@bestEmployee')->name('users.best_employee');
Route::resource('teams','TeamsController');
Route::resource('tasks','TasksController');
Route::resource('users','UsersController');
Route::put('/tasks/{task}/resolve','TasksController@resolve')->name('tasks.resolve');
Route::put('/tasks/{task}/giveup','TasksController@giveUp')->name('tasks.giveup');
Route::put('/tasks/{task}/approve','TasksController@approveTask')->name('tasks.approve');
Route::put('/tasks/{task}/unapprove','TasksController@unapproveTask')->name('tasks.unapprove');
Route::put('/tasks/{task}/update_self_assign','TasksController@updateSelfReassignTask')->name('tasks.update_self_assign');
Route::put('/tasks/{task}/system_assign','TasksController@systemReassignTask')->name('tasks.system_assign');

