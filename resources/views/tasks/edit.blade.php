@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        Edit Task
    </div>
    <div class="card-body">
        <form action="{{route('tasks.update',$task->id)}}" method = "POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text"
                    value = "{{old('name',$task->name)}}"
                    class = "form-control" @error('name') is-valid @enderror name = "name" id = "name">
                @error('name')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="assigned_to">Assign to team member</label>
                <select name="assigned_to" id="assigned_to" class = "form-control">
                <option value="{{$task->assigned_to}}">{{$task->user->name}}</option>
                    @foreach($users as $user)
                        @if(!($user->role === 'leader'))
                            @if($task->assigned_to != $user->id)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endif
                        @endif
                    @endforeach
                </select>
                @error('assigned_to')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>


            <div class="form-group">
                <button class = "btn btn-success" type="submit">Edit Task</button>
            </div>
        </form>
    </div>
</div>
@endsection