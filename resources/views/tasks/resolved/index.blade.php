@extends('layouts.app')
@section('content')

<div class="d-flex justify-content-end mb-3">
</div>
<div class="card">
<div class="card-header">
        Resolved Tasks
</div>
    <div class="card-body">
        @if($tasks->count()>0)
            <table class ="table table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Assigned To</th>
                    <th>Resolved At</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td>
                                {{ $task->name}}
                            </td>
                            <td>
                                {{ $task->user->name}}
                            </td>
                            <td>
                                {{ $task->statuses()->latest()->first()->resolved_at}}
                            </td>
                            <td>
                                @can('canEditDeleteCreateTask',$task->user)
                                    <form action="{{ route('tasks.approve',$task)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button class = "btn btn-success btn-sm" type="submit">Approve</button>
                                    </form>
                                    <form action="{{ route('tasks.unapprove',$task)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class = "btn btn-danger btn-sm">UnApprove</button>
                                    </form>
                                    
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>Nothing To Show</p>
        @endif
    </div>
</div>
@endsection