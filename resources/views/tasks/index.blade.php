@extends('layouts.app')
@section('content')

<div class="d-flex justify-content-end mb-3">
    @can('canEditDeleteCreateTask',auth()->user())
        <a href="{{ route('tasks.create') }}" class= "btn btn-primary mr-3">Create Task</a>
        <a href="{{route('tasks.resolved')}}" class = "btn btn-secondary mr-3">Resolved Tasks</a>
        <a href="{{route('tasks.approved')}}" class = "btn btn-success">Approved Tasks</a>
    @endcan
</div>
<div class="card">
<div class="card-header">
    @if(auth()->user()->role === 'member')
        Task Created By {{auth()->user()->getLeader(auth()->user())[0]['name']}}
    @else
        Tasks
    @endif
</div>
    <div class="card-body">
        @if($tasks->count()>0)
            <table class ="table table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Assigned To</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        @if($task->status->gave_up_at === null)    
                            <tr>
                                <td>
                                    {{ $task->name}}
                                </td>
                                <td>
                                    {{ $task->user->name}}
                                </td>
                                <td>
                                    @can('canEditDeleteCreateTask',$task->user)
                                        <a href="{{ route('tasks.edit',$task->id)}}" class = "btn btn-primary btn-sm">Edit</a>
                                        <a href="#" class = "btn btn-danger btn-sm" data-toggle = "modal" data-target = "#deleteModal"
                                        onclick="displayModalForm({{$task}})">Delete</a>
                                    @else
                                        <div class="d-flex">
                                            <form action="{{ route('tasks.resolve',$task->id)}}" method="POST" class = "mr-3">
                                                @csrf
                                                @method('PUT')
                                                <button type = "submit" class = "btn btn-primary btn-sm">Resolve</button>
                                            </form>
                                            <form action="{{ route('tasks.giveup',$task->id)}}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <button type = "submit" class = "btn btn-danger btn-sm">Give Up</button>
                                            </form>
                                        </div>
                                    @endcan
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        @else
            <p>Nothing To Show</p>
        @endif
    </div>
</div>

@can('canEditDeleteCreateTask',auth()->user())
    <div class="card mt-4">
        <div class="card-header">
            Gave Up Tasks
        </div>
        <div class="card-body">
            @if($tasks->count()>0)
                <table class ="table table-bordered">
                    <thead>
                        <th>Name</th>
                        <th>Assigned To</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            @if($task->status->gave_up_at !== null)
                                <tr>
                                    <td>
                                        {{ $task->name}}
                                    </td>
                                    <td>
                                        {{ $task->user->name}}
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="{{ route('tasks.self_assign',$task->id)}}" type = "submit" class = "btn btn-info btn-sm mr-3">Self Assign</a>
                                            <form action="{{ route('tasks.system_assign',$task->id)}}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <button type = "submit" class = "btn btn-warning btn-sm">System Assign</button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>Nothing To Show</p>
            @endif
        </div>
    </div>
@endcan

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form action="" method = "POST" id = "deleteForm">
        @csrf
        @method('DELETE')
        <div class="modal-body">
            <p>Are you sure you want to delete Task?</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Delete Task</button>
        </div>
    </form>
  </div>
</div>
</div>
@endsection

@section('page-level-scripts')
<script type = "text/javascript">
function displayModalForm($task){
    alert('reached');
    var url = "/tasks/" + $task.id;
    $("#deleteForm").attr('action',url);
}
</script>
@endsection