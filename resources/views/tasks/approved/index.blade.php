@extends('layouts.app')
@section('content')

<div class="d-flex justify-content-end mb-3">
</div>
<div class="card">
<div class="card-header">
        Approved Tasks
</div>
    <div class="card-body">
        @if($tasks->count()>0)
            <table class ="table table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Assigned To</th>
                    <th>Resolved At</th>
                    <th>Approved At</th>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td>
                                {{ $task->name}}
                            </td>
                            <td>
                                {{ $task->user->name}}
                            </td>
                            <td>
                                {{ $task->status->resolved_at}}
                            </td>
                            <td>
                                {{ $task->status->approved_at}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>Nothing To Show</p>
        @endif
    </div>
</div>
@endsection