@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        Add Task
    </div>
    <div class="card-body">
        <form action="{{route('tasks.store')}}" method = "POST">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text"
                    value = "{{old('name')}}"
                    class = "form-control" @error('name') is-valid @enderror name = "name" id = "name">
                @error('name')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="assigned_to">Assign to team member</label>
                <select name="assigned_to" id="assigned_to" class = "form-control">
                    @foreach($users as $user)
                        @if(!($user->role === 'leader'))
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endif
                    @endforeach
                </select>
                @error('assigned_to')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>


            <div class="form-group">
                <button class = "btn btn-success" type="submit">Add Task</button>
            </div>
        </form>
    </div>
</div>
@endsection