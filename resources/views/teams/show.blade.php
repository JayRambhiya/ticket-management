@extends('layouts.app')
@section('content')
<div class="card">
    <div class="card-header">Team Members</div>
    <div class="card-body">
        <table class ="table table-bordered">
            <thead>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            {{ $user->name}}
                        </td>
                        <td>
                            {{ $user->email}}  
                        </td>
                        <td>
                            {{ $user->role}}  
                        </td>

                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection