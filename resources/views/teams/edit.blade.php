@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Edit Team</div>
        <div class="card-body">
            <form action="{{ route('teams.update', $team->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"
                    value="{{old('name', $team->name)}}"
                    class="form-control @error('name') is-invalid @enderror"
                    name="name" id="name">
                    @error('name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Edit Team</button>
                </div>
            </form>
        </div>
    </div>
@endsection