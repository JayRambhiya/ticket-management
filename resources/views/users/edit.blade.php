@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        Edit User
    </div>
    <div class="card-body">
        <form action="{{route('users.update',$user->id)}}" method = "POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text"
                    value = "{{old('name',$user->name)}}"
                    class = "form-control" @error('name') is-valid @enderror name = "name" id = "name">
                @error('name')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="text"
                    value = "{{old('email',$user->email)}}"
                    class = "form-control" @error('email') is-valid @enderror name = "email" id = "email">
                @error('email')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="team_id">Assign to Team</label>
                <select name="team_id" id="team_id" class = "form-control">
                    @foreach($teams as $team)
                        @if($user->team_id === $team->id)
                            <option value="{{$team->id}}" selected>{{$team->name}}</option>
                        @else
                            <option value="{{$team->id}}">{{$team->name}}</option>
                        @endif
                    @endforeach
                </select>
                @error('team_id')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>


            <div class="form-group">
                <button class = "btn btn-success" type="submit">Edit user</button>
            </div>
        </form>
    </div>
</div>
@endsection