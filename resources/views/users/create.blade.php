@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        Add User
    </div>
    <div class="card-body">
        <form action="{{route('users.store')}}" method = "POST">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text"
                    value = "{{old('name')}}"
                    class = "form-control" @error('name') is-valid @enderror name = "name" id = "name">
                @error('name')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="text"
                    value = "{{old('email')}}"
                    class = "form-control" @error('email') is-valid @enderror name = "email" id = "email">
                @error('email')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password"
                    value = "{{old('password')}}"
                    class = "form-control" @error('password') is-valid @enderror name = "password" id = "password">
                @error('password')
                    <p class = "text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">
                <button class = "btn btn-success" type="submit">Add User</button>
            </div>
        </form>
    </div>
</div>
@endsection