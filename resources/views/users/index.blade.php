@extends('layouts.app')
@section('content')

<div class="d-flex justify-content-end mb-3">
    <a href="{{ route('users.create') }}" class= "btn btn-primary">Create User</a>
</div>
<div class="card">
    <div class="card-header">Users</div>
    <div class="card-body">
        <table class ="table table-bordered">
            <thead>
                <th>Name</th>
                <th>Email</th>
                <th>Team</th>
                <th>Role</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach($users as $user)
                    @if(!($user->id === auth()->id()))
                        <tr>
                            <td>
                                {{ $user->name}}
                            </td>
                            <td>
                                {{ $user->email}}
                            </td>
                            <td>
                                @if($user->team_id === null)
                                    {{ 'Still Not Assigned'}}
                                @else
                                    {{ $user->team->name}}
                                @endif
                            </td>
                            <td>
                                {{ $user->role}}
                            </td>
                            <td>
                                <a href="{{ route('users.edit',$user->id)}}" class = "btn btn-primary btn-sm">{{$user->team_id ? 'Edit' : 'Assign Team'}}</a>
                                <a href="#" class = "btn btn-danger btn-sm" data-toggle = "modal" data-target = "#deleteModal"
                                onclick="displayModalForm({{$user}})">Delete</a>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form action="" method = "POST" id = "deleteForm">
        @csrf
        @method('DELETE')
        <div class="modal-body">
            <p>Are you sure you want to delete User?</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Delete User</button>
        </div>
    </form>
  </div>
</div>
</div>
@endsection

@section('page-level-scripts')
<script type = "text/javascript">
function displayModalForm($user){
    alert('reached');
    var url = "/users/" + $user.id;
    $("#deleteForm").attr('action',url);
}
</script>
@endsection